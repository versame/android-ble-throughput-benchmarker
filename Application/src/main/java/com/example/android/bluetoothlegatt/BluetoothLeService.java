/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetoothlegatt;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

class BluetoothTask {
    BluetoothGattCharacteristic mCharacteristic = null;
    BluetoothGattDescriptor mDescriptor = null;
    BluetoothGattCharacteristic characteristic_to_write_to = null;
    byte[] byte_ary_to_write = null;

    BluetoothTask(BluetoothGattCharacteristic characteristic) {
        mCharacteristic = characteristic;
    }

    BluetoothTask(BluetoothGattDescriptor descriptor) {
        mDescriptor = descriptor;
    }

    BluetoothTask(BluetoothGattCharacteristic characteristic, byte[] bytes_to_write) {
        characteristic_to_write_to = characteristic;
        byte_ary_to_write = bytes_to_write;
    }

    boolean execute(BluetoothGatt gatt) {
        if (mCharacteristic != null) {
            return gatt.readCharacteristic(mCharacteristic);
        } else if (mDescriptor != null) {
            return gatt.readDescriptor(mDescriptor);
        } else if (byte_ary_to_write != null) {
            characteristic_to_write_to.setValue(byte_ary_to_write);
            while (gatt.writeCharacteristic(characteristic_to_write_to) == false) {
            }
            ;
            return true;
        } else {
            return false;
        }
    }
}

class BluetoothTaskQueue {
    private boolean mInProgress = false;
    BluetoothGatt mBluetoothGatt;
    ConcurrentLinkedQueue<BluetoothTask> mQueue = new ConcurrentLinkedQueue<BluetoothTask>();


    BluetoothTaskQueue(BluetoothGatt gatt) {
        mBluetoothGatt = gatt;
    }

    void next() {
        if (mQueue.size() > 0) {
            mInProgress = true;
            mQueue.poll().execute(mBluetoothGatt);
        } else {
            mInProgress = false;
        }
    }

    boolean add(BluetoothTask task) {
        boolean result = mQueue.add(task);
        if (mInProgress == false) {
            next();
        }
        return result;
    }
}

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */
public class BluetoothLeService extends Service {
    private final static String TAG = BluetoothLeService.class.getSimpleName();

    public BluetoothGattCharacteristic RXChar;
    private double loopback_count = 0;
    private double total_time_taken = 0;
    private double time_taken_diff = 0;
    private double last_check_timestamp = 0;
    private double send_timestamp = 0;
    private double receive_timestamp = 0;
    private double send_datestamp = 0;
    private double receive_datestamp = 0;

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;
    private BluetoothTaskQueue mQueue;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    public final static String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String ACTION_BAT_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_BAT_DATA_AVAILABLE";

    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";
    public final static String EXTRA_VALUE =
            "com.example.bluetooth.le.EXTRA_VALUE";
    public final static String EXTRA_LOCATION =
            "com.example.bluetooth.le.EXTRA_LOCATION";

    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = ACTION_GATT_CONNECTED;
                mConnectionState = STATE_CONNECTED;
                broadcastUpdate(intentAction);
                Log.i(TAG, "Connected to GATT server.");
                // Attempts to discover services after successful connection.
                Log.i(TAG, "Attempting to start service discovery:" +
                        mBluetoothGatt.discoverServices());

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                mConnectionState = STATE_DISCONNECTED;
                Log.i(TAG, "Disconnected from GATT server.");
                broadcastUpdate(intentAction);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            Log.d("ble-devctrl", String.format("onCharacteristicRead() called for characteristic %s", characteristic));

            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }

            mQueue.next();
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt,
                                     BluetoothGattDescriptor descriptor,
                                     int status) {
            Log.d("ble-devctrl", String.format("onDescriptorRead() called for descriptor %s", descriptor));

            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, descriptor.getCharacteristic());
            }

            mQueue.next();
        }


        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
//            Log.d("ble-devctrl", String.format("onCharacteristicWrite() called for characteristic %s", characteristic));
            mQueue.next();
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            // For all unrelated stuff ignore the writes
            if (SampleGattAttributes.TX.equals(characteristic.getUuid().toString())) {
                receive_timestamp = (new Date()).getTime();
                final byte[] data = characteristic.getValue();
                if (data[data.length - 1] == 0) {
                    final StringBuilder stringBuilder = new StringBuilder(data.length);
                    for (byte byteChar : data)
                        stringBuilder.append(String.format("%02X ", byteChar));
//                    Log.d("Rushd Stuff", stringBuilder.toString());
                    loopback_count++;
                    total_time_taken += (receive_timestamp - send_timestamp);
                    if ((loopback_count % 10) == 0) {
                        time_taken_diff = total_time_taken - last_check_timestamp;
                        last_check_timestamp = total_time_taken;
                        Log.i("Rushd Stuff", "Total loopbacks so far:" + loopback_count + " total_time_taken: " + total_time_taken + " milliseconds");
                        Log.i("Rushd ", "time taken for last 100 loopbacks:" + time_taken_diff);
                        final double avg = (loopback_count * 1000) / total_time_taken;
                        Log.i("Rushd ", "Average loopbacks per second:" + avg);

                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),
                                        "Average loopbacks per second: " + avg, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    writeToRx256BytesLoopbackMsg();
                }
            }
        }
    };

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {


    }

    public class LocalBinder extends Binder {
        BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The connection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;

        mQueue = new BluetoothTaskQueue(mBluetoothGatt);

        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        Log.d(TAG, String.format("readCharacteristic(%s)", characteristic));

        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }

        mQueue.add(new BluetoothTask(characteristic));
    }


    /**
     * Request a read on a given {@code BluetoothGattDescriptor}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onDescriptorRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param descriptor The descriptor to read from.
     */
    public void readDescriptor(BluetoothGattDescriptor descriptor) {
        Log.d(TAG, String.format("readDescriptor(%s)", descriptor));

        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }

        // HACK: probably a race condition doing it this way
        mQueue.add(new BluetoothTask(descriptor));
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled        If true, enable notification.  False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(descriptor);
    }

    /**
     * writes to Rx characteristic characteristic 256 bytes loopback msg
     *
     * @return A {boolean} whether it wrote it successfully or not
     */
    public boolean writeToRx256BytesLoopbackMsg() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w("Rushd", "BluetoothAdapter not initialized");
            return true;
        }
        byte[] value1 = {(byte) 255, (byte) 255, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};
        byte[] value2 = {19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38};
        byte[] value3 = {39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58};
        byte[] value4 = {59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78};
        byte[] value5 = {79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98};
        byte[] value6 = {99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118};
        byte[] value7 = {119, 120, 121, 122, 123, 124, 125, 126, 127, (byte) 128, (byte) 129, (byte) 130, (byte) 131, (byte) 132, (byte) 133, (byte) 134, (byte) 135, (byte) 136, (byte) 137, (byte) 138};
        byte[] value8 = {(byte) 139, (byte) 140, (byte) 141, (byte) 142, (byte) 143, (byte) 144, (byte) 145, (byte) 146, (byte) 147, (byte) 148, (byte) 149, (byte) 150, (byte) 151, (byte) 152, (byte) 153, (byte) 154, (byte) 155, (byte) 156, (byte) 157, (byte) 158};
        byte[] value9 = {(byte) 159, (byte) 160, (byte) 161, (byte) 162, (byte) 163, (byte) 164, (byte) 165, (byte) 166, (byte) 167, (byte) 168, (byte) 169, (byte) 170, (byte) 171, (byte) 172, (byte) 173, (byte) 174, (byte) 175, (byte) 176, (byte) 177, (byte) 178};
        byte[] value10 = {(byte) 179, (byte) 180, (byte) 181, (byte) 182, (byte) 183, (byte) 184, (byte) 185, (byte) 186, (byte) 187, (byte) 188, (byte) 189, (byte) 190, (byte) 191, (byte) 192, (byte) 193, (byte) 194, (byte) 195, (byte) 196, (byte) 197, (byte) 198};
        byte[] value11 = {(byte) 199, (byte) 200, (byte) 201, (byte) 202, (byte) 203, (byte) 204, (byte) 205, (byte) 206, (byte) 207, (byte) 208, (byte) 209, (byte) 210, (byte) 211, (byte) 212, (byte) 213, (byte) 214, (byte) 215, (byte) 216, (byte) 217, (byte) 218};
        byte[] value12 = {(byte) 219, (byte) 220, (byte) 221, (byte) 222, (byte) 223, (byte) 224, (byte) 225, (byte) 226, (byte) 227, (byte) 228, (byte) 229, (byte) 230, (byte) 231, (byte) 232, (byte) 233, (byte) 234, (byte) 235, (byte) 236, (byte) 237, (byte) 238};
        byte[] value13 = {(byte) 239, (byte) 240, (byte) 241, (byte) 242, (byte) 243, (byte) 244, (byte) 245, (byte) 246, (byte) 247, (byte) 248, (byte) 249, (byte) 250, (byte) 251, (byte) 252, (byte) 253, 0};

        send_timestamp = (new Date()).getTime();
        mQueue.add(new BluetoothTask(RXChar, value1));
        mQueue.add(new BluetoothTask(RXChar, value2));
        mQueue.add(new BluetoothTask(RXChar, value3));
        mQueue.add(new BluetoothTask(RXChar, value4));
        mQueue.add(new BluetoothTask(RXChar, value5));
        mQueue.add(new BluetoothTask(RXChar, value6));
        mQueue.add(new BluetoothTask(RXChar, value7));
        mQueue.add(new BluetoothTask(RXChar, value8));
        mQueue.add(new BluetoothTask(RXChar, value9));
        mQueue.add(new BluetoothTask(RXChar, value10));
        mQueue.add(new BluetoothTask(RXChar, value11));
        mQueue.add(new BluetoothTask(RXChar, value12));
        mQueue.add(new BluetoothTask(RXChar, value13));

//        RXChar.setValue(value1);
//        send_timestamp = (new Date()).getTime();
//        while(mBluetoothGatt.writeCharacteristic(RXChar) == false) {};
//        RXChar.setValue(value2);
//        while(mBluetoothGatt.writeCharacteristic(RXChar) == false) {};
//        RXChar.setValue(value3);
//        while(mBluetoothGatt.writeCharacteristic(RXChar) == false) {};
//        RXChar.setValue(value4);
//        while(mBluetoothGatt.writeCharacteristic(RXChar) == false) {};
//        RXChar.setValue(value5);
//        while(mBluetoothGatt.writeCharacteristic(RXChar) == false) {};
//        RXChar.setValue(value6);
//        while(mBluetoothGatt.writeCharacteristic(RXChar) == false) {};
//        RXChar.setValue(value7);
//        while(mBluetoothGatt.writeCharacteristic(RXChar) == false) {};
//        RXChar.setValue(value8);
//        while(mBluetoothGatt.writeCharacteristic(RXChar) == false) {};
//        RXChar.setValue(value9);
//        while(mBluetoothGatt.writeCharacteristic(RXChar) == false) {};
//        RXChar.setValue(value10);
//        while(mBluetoothGatt.writeCharacteristic(RXChar) == false) {};
//        RXChar.setValue(value11);
//        while(mBluetoothGatt.writeCharacteristic(RXChar) == false) {};
//        RXChar.setValue(value12);
//        while(mBluetoothGatt.writeCharacteristic(RXChar) == false) {};
//        RXChar.setValue(value13);
//        while(mBluetoothGatt.writeCharacteristic(RXChar) == false) {};
        return true;
    }


    /**
     * writes to Rx characteristic characteristic 20 bytes loopback msg
     *
     * @return A {boolean} whether it wrote it successfully or not
     */
    public boolean writeToRx20BytesLoopbackMsg() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w("Rushd", "BluetoothAdapter not initialized");
            return true;
        }
        byte[] value = {0x13, (byte) 0xFF, 0x01, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 0};
        RXChar.setValue(value);
        send_timestamp = (new Date()).getTime();
        return mBluetoothGatt.writeCharacteristic(RXChar);
    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null) return null;

        return mBluetoothGatt.getServices();
    }
}
